
The internet of the 2020's is a nightmare dystopian hellscape only foreseen by the likes of
George Orwell and HP Lovecraft.   You will require 20 cm of solid tungsten plating and a
high-intensity coherent X-ray light source to have any hope of browsing it without dying
of syphilis soon after.

[`alternative-front-ends`](https://github.com/mendel5/alternative-front-ends) is a helpful list of
front-ends for popular services.

Here are some other tools that may help keep you sane just a little longer.

## Mail Client and Services
- [`neomutt`](https://neomutt.org/) I haven't really bothered to set this up but it seems
    nice
- [`simplelogin`](https://simplelogin.io/) provides randomly generated email aliases to protect your
    email address.

## Browsers
- [`firefox`](https://www.mozilla.org/en-US/firefox/) is still the browser you should
    probably be using.  open source since forever, it has the best communities around it.
- [`brave`](https://brave.com/) if you have to use a chromium browser, use brave. there is
    still some pretty dubious stuff going on surrounding an innovative and commendable,
    but ultimately doomed business model, but it's mostly a good browser.  by far the most
    objectionable thing is the use of promotional images in new tabs by default

## Browser Extensions
- [`tridactyl`](https://github.com/tridactyl/tridactyl) is an incredibly powerful vim-like user
    interface layer for firefox.  Of course, somethings still break it because browsers.
- [`vimium`](https://github.com/philc/vimium) the closest thing you'll get to `tridactyl` for
    chromium browsers, but vastly inferior to `tridactyl`.
- [`uBlock Origin`](https://github.com/gorhill/uBlock) the best add blocker
- [`User-Agent Switcher`](https://gitlab.com/ntninja/user-agent-switcher) because lots of
    sites sabotage themselves just because you're not using Google
- [`ClearURLs`](https://github.com/ClearURLs) cleans up URL's, a sometimes overlooked
    piece of the surveillance infrastructure
- [`Cookie-AutoDelete`](https://github.com/Cookie-AutoDelete/Cookie-AutoDelete) one of the
    best extensions. whitelist the few cookies you are willing to allow, delete the rest
    automatically as frequently as you want. also good to use in conjunction with firefox
    container tabs.
- [`privacy-redirect`](https://github.com/SimonBrazell/privacy-redirect) re-direct web services to
    various non-dystopian front-ends (e.g. invidious, nitter).
- [`privacypossum`](https://github.com/cowlicks/privacypossum) does some cookie blocking
    that is entirely redundant with uBlock and firefox and brave built-in protection, but
    also does some fingerprint protection.
- [`decentraleyes`](https://decentraleyes.org/) re-directs "content delivery network" requests to
    local sources where possible to prevent spying.
- [`TabSessionManager`](https://github.com/sienori/Tab-Session-Manager) I'm definitely not a
    "trillions of tabs" person but these days this seems like basic functionality that all browsers
    should have, though it's usefulness is severely undermined by the inability to control it with
    `tridactyl`.

## Media Sources
- [`invidious`](https://github.com/iv-org/invidious) is a YouTube front-end written in
    crystal.
- [`FreeTube`](https://github.com/FreeTubeApp/FreeTube) is another YouTube front-end.
- [`NewPipe`](https://github.com/TeamNewPipe/NewPipe) a really good YouTube front-end
    that's sadly only available as an Android app. definitely best option right now for
    mobile though
- [`nitter`](https://nitter.net/) a front-end for twitter. not very useful as twitter is a
    cesspit, but sometimes you'll get links there.
- [`youtube-dl`](https://github.com/ytdl-org/youtube-dl). Makes a nice combination with `mpv`.
- [`mps-youtube`](https://github.com/mps-youtube/mps-youtube) a terminal based YouTube
    front-end. uses `youtube-dl` and `mpv`.
- [`peertube`](https://github.com/Chocobozzz/PeerTube) a P2P video hosting network.
    Recommend the [privacytools.io instance](https://tube.privacytools.io).
- [`~cadence/tube`](https://sr.ht/~cadence/tube/) uses CloudTube with `second` as a
    YouTube front-end.  instance [here](https://tube.cadence.moe/)

## Git
Though so Microsoft has not done anything outrageous with their stewardship of Github, the
service has nonetheless shown itself to be terrifyingly vulnerable to censorship.
Ultimately we will need a P2P E2EE replacement for Github and the like, but for now, here
are some alternative git hosting services.
- [`GitLab`](https://gitlab.com/) where this is hosted. I would say the features here are
    even better than github
- [`sourcehut`](https://sr.ht/) a git hosting service with a wonderfully minimalistic web
    front-end. it is a paid service, but worth keeping an eye on.
- [`gitea`](https://gitea.io/) seems to be an open source clone of github. access the main
    instance [here](https://gitea.com/gitea/).
- [`radicle`](https://radicle.xyz/) is a P2P E2EE git protocol. currently in the nacent
    stages of development, but looks promising


