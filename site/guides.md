
Some useful guides

- [Beej's Guide to Unix IPC](https://beej.us/guide/bgipc/html/single/bgipc.html)
- [Beej's Guide to Network Programming](https://beej.us/guide/bgnet/)
- [GNU on
    Pipes](https://www.gnu.org/software/libc/manual/html_node/Pipes-and-FIFOs.html#Pipes-and-FIFOs)
- [GNU on
    Sockets](https://www.gnu.org/software/libc/manual/html_node/Sockets.html#Sockets)
- [High Performance Browser Networking](https://hpbn.co/) (don't be scared off by the
    name: this is a fantastic online textbook and most of it does not specifically involve
    browsers)
- [nvim-lua-guide](https://github.com/nanotee/nvim-lua-guide) on usage of Lua instead of
    vimscript with neovim.
- [Rewritten in Rust](https://zaiste.net/posts/shell-commands-rust/) a helpful list of modernized
    command line tools written in rust.
- https://linmobapps.frama.io/ a helpful list of mobile linux apps.
