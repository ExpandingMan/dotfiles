@def title = "the dotfiles of the Expanding Man"

# Expanding Linux
This is the linux configuration of [Expanding Man](https://expandingman.xyz).

I use [Manjaro Linux](https://manjaro.org/) wherever possible.  As of writing, I do not have a
specialized installer, I typically start with the [i3 minimal
install](https://manjaro.org/downloads/community/i3/) and add what I need from there.

I have separate repositories for the main components.
- [Fish shell](https://gitlab.com/ExpandingMan/fishconfig)
- [Qtile Window Manager](https://gitlab.com/ExpandingMan/qtileconfig)
- [Neovim Editor](https://gitlab.com/ExpandingMan/neovimconfig)
- Other configuration is found in the [main dotfiles repo](https://gitlab.com/ExpandingMan/dotfiles)

## Details

### window manager
I recently switched to the [Qtile](http://www.qtile.org/) window manager.  Previously I had used
`i3` exclusively.  While the simplicity of `i3` was quite appealing, I found it deficient mainly
because of two reasons.  First, the lack of
"[layouts](https://docs.qtile.org/en/stable/manual/ref/layouts.html)", a feature of many of the more
recent window managers made having $\gtrsim 2$ windows on a single screen simultaneously all but
unmanageable.  Obviously, this is a huge problem for a window manager, but because I usually have
multiple screens anyway and so frequently would use `i3`'s tabs, I did not find it prohibitive.
Second, though `i3`'s IPC allows a great deal of flexibility, lack of sophisticated scripting
functionality was limiting.

The main reason for the choice of Qtile is that it is apparently the only window manager which
supports both X11 and Wayland with practically no switching overhead.  I've mostly been stuck on X11
becuase most of my machines have nvidia GPU's but I want to move everything over to Wayland whenever
this becomes practical (hopefully by the next generation of video cards AMD will be strictly better).

I am not fond of Python, in which Qtile is written, though a window manager is an infinitely more
sane application for the language than, say, scientific/numerical computing.  I've been toying with
the idea of writing a window manager in Julia, especially since the [backend source code of
qtile](https://github.com/qtile/qtile/tree/master/libqtile/backend) provides such an excellent
starting point for writing a WM that supports both X11 and Wayland, but this has not yet proceeded
past idle speculation.

### terminal
I mostly use [alacritty](https://github.com/alacritty/alacritty), it is very lightweight and fast
and its rendering looks nice.

More recently I've been experimenting with [`wezterm`](https://github.com/wez/wezterm) and will
likely switch, since it has some much coveted featurees (especially
[sixel](https://en.wikipedia.org/wiki/Sixel) support) but it still occasionally has some weird
problems with window managers so I haven't fully switched over yet.

The shell I use is [`fish`](https://fishshell.com/) with [`starship`](https://starship.rs/) prompt.
My `fish` config kept in [this repo](https://gitlab.com/ExpandingMan/fishconfig).  I have the
ability to bootstrap into fish with reasonable defaults for `bash` and `zsh`, see below.

Text editing is done with [neovim](https://github.com/neovim/neovim) with a setup which is written
entirely in Lua and which has [its own repository](https://gitlab.com/ExpandingMan/neovimconfig).

### colors and appearance
I use the [dracula theme](https://github.com/dracula/dracula-theme) for pretty much
everything, including this site.  I landed on this after having used solarized dark for a
while but ultimately being driven near nausea from the dull, faded colors.  I enjoy neon
color schemes, of which dracula is an example, though not necessarily the best.

My use of dracula everywhere may lead you to conclude
that I am in love with this theme.  This is not the case.  However, after multiple
disastrous attempts to construct my own color scheme, I have a deep appreciation for how
incredibly hard it is to create a good color scheme, particularly one you intend to use
for code highlighting.  At some point I'd like to either create or obtain a neon
colorscheme I like better but for now, dracula it is.

I use the [JuliaMono](https://github.com/cormullion/juliamono) font for all monospaced fonts.  This
font has impeccable unicode support and clearly distinguished delimiters.  For non-monospace fonts,
I use [Computer Modern](https://tug.org/FontCatalogue/computermodern/) font (serif where possible)
which is the LaTeX font.

### julia
I make frequent use of my favorite programming language, [Julia](https://julialang.org/).  Julia is
a fast, garbage-collected language with a novel runtime.  It is developed largely with scientific
and numerical computing in mind, and as such takes mathematical abstraction delightfully seriously.
Despite its scientific roots, it is a true general purpose language.  I normally use the Julia REPL
either alongside or within `nvim`, with [Revise.jl](https://github.com/timholy/Revise.jl).

### the Great Shell Bootstrap
I use `fish` which is not installed by default on most systems, but I also `ssh` into other systems
or use docker images relatively frequently.  Not having a properly configured shell can be scary, so
I maintain a configuration for `bash` and `zsh`.  The shell configuration is stratified thusly:
- `.profile` exports important variables that *must* be available to the window manager.
- `.bashrc` contains my primary configuration for `bash` and `zsh`.
- `.zshrc` contains basically nothing not present for `bash`.
- `.config/fish/` contains the fish shell configuration from the
    [fishconfig](https://gitlab.com/ExpandingMan/fishconfig) repo.

Each strata sources the one below it.  Additionally, I use quite a few command line tools written in
rust which are not initially present on most systems, and my configuration ensures they are present
before setting them as default.  Therefore, I can quickly achieve a reasonable setup on most systems
in `bash`.  On Arch I typically install the rust tools via the systme package manager, on other
distros it's usually better to use `rustup` and `cargo`.

## installer

\fig{/assets/installer.gif}

I have written an installer in Julia.  You should enter the installer with `install.sh`.
The installer will launch you into a Julia REPL with various functions available to
facilitate installation of various programs.  When run interactively, various prompts will
be given so that you can safely install new configs with full control over what gets
overwritten.  You can see a list of programs the installer can install and configure with
`install()`, or see the list
[here](https://gitlab.com/ExpandingMan/dotfiles/-/tree/master/Installer/src/programs).

An increasingly large number of programs I make use of are written in rust and are most easily
installed with `cargo install`.  I typically install rust through `pacman`, but for non-Arch it can
be bootstrapped through the installer by installing the `rustup` init script via
`install(Programs.rust)`.

> **NOTE**: The installer is intended for installing configurations and programs that
> some form of special setup.  In cases where the package manager is sufficient for
> installation, it is much preferred.

You can install Julia itself to `/opt/julia` with `sudo ./install.sh julia`
