
This is a list of native software that I maintain for my own reference.

If a program appears here, it means it's useful enough for me to use it, it does not
necessarily mean it's great (though many are).

**Resources:**
- [privacyguides.org](https://privacyguides.org)
- [awesome-rust](https://github.com/rust-unofficial/awesome-rust)
- [awesome-tui](https://github.com/rothgar/awesome-tuis)
- [gamingonlinux.com](https://www.gamingonlinux.com/)


## Terminal and Shell
- [`alacritty`](https://github.com/alacritty/alacritty) a configurable, fast terminal
    written in rust
- [`prezto`](https://github.com/sorin-ionescu/prezto) is a `zsh` configuration framework
    that I like way better than `oh-my-zsh`.  minimal enough to be fast but still with
    tons of useful features.
- [`neovim`](https://github.com/neovim/neovim) the editor I use for everything
- [`nushell`](https://github.com/nushell/nushell) an interesting new approach to a shell.
    not sold on it yet.
- [`bat`](https://github.com/sharkdp/bat) a file dumper like `cat`. particularly useful
    for git diffs and displaying non-printable data.
- [`tokei`](https://github.com/XAMPPRocky/tokei) is `wc -l` on steroids.
- [`tealdeer`](https://github.com/dbrgn/tealdeer) an incredibly useful summary of `man` pages.
    Executable is normally called `tldr`.
- [`grex`](https://github.com/pemistahl/grex) generate regex strings from examples.
- [`zoxide`](https://github.com/ajeetdsouza/zoxide) fast directory switcher built around saving
    a list of directories to a database.
- [`fd`](https://github.com/sharkdp/fd) another rust tool, this time for finding files, it is
    unreasonably fast.
- [`starship`](https://starship.rs/) generates terminal prompts which are incredibly easy to
    configure. Currently used in my `.zshrc` (together with `prezto`).
- [`ueberzug`](https://github.com/seebye/ueberzug) an extremely hacky method for drawing images
    *over* terminal windows. Vastly inferior alternative to sixel, but works on anything (sort of).
- [`screen`](https://wiki.archlinux.org/title/GNU_Screen) is about the simplest option for session
    management, doesn't support true color though.
- [`zellij`](https://github.com/zellij-org/zellij) is a modern session manager which is much closer
    to feature parity with `tmux` than `screen`.  Configuration options are a bit lacking.

## Window and Display Management
- [`i3`](https://i3wm.org) the "standard" window manager.  not necessarily the best, but
    it's very available, customizable, can be installed and runs on anything.
- [`swaywm`](https://swaywm.org/) the Wayland version of `i3`.  don't try running on
    nvidia
- [`arandr`](https://www.archlinux.org/packages/community-staging/any/arandr/) is a GUI
    that let's you arange and configure displays, and save an `xrandr` script that
    executes your new config.  pretty much mandatory if you are using a window manager
- [`rofi`](https://github.com/davatorium/rofi) configurable launcher for windows managers
- [`picom`](https://github.com/yshui/picom) once known as compton, for making windows
    transparent.  I was never happy with how this looked and don't use it now.
- [`polybar`](https://wiki.archlinux.org/index.php/Polybar) a status bar for window
    managers.  flexible with lots of features.  a pain in the ass to build on systems that
    don't already package it (debian)
- [`dunst`](https://dunst-project.org/) a minimal but highly configuration notification
    daemon.

## Network
- `nmcli` is the CLI to
    [NetworkManager](https://wiki.archlinux.org/index.php/NetworkManager) the most common
    linux network manager.
- `nmtui` is a TUI to `NetworkManager`.  useful especially for adding connections
- [`bandwhich`](https://github.com/imsnif/bandwhich) a nifty network utilization monitor.  Very
    helpful for knowing what programs are doing.

## File System
- [`ranger`](https://wiki.archlinux.org/index.php/Ranger) is a TUI file browser. if set up
    correctly can display full images. useful if you need to quickly browse through files
    while viewing the contents.

## Package Management
- [`pacui`](https://github.com/excalibur1234/pacui) an excellent TUI front-end for
    `pacman` and the AUR. particularly useful when searching for packages or even which
    files belong to which package.

## System
- [`inxi`](https://github.com/smxi/inxi) an invaluable tool for getting system
    information. obviates the need for `lscpu` and all those other `ls*` commands.
- [`bpytop`](https://github.com/aristocratos/bpytop) pretty TUI system monitor.  great for
    hardware status, not as good as `htop` for managing processes
- [`bottom`](https://github.com/ClementTsang/bottom) another powerful alternative to `htop` written
    in rust.  Executable is normally called `btm`.
- [`nvtop`](https://github.com/Syllo/nvtop) like `htop` but for nvidia GPU's.  Includes a nice line
    graph.
- [`kmon`](https://github.com/orhun/kmon) a TUI kernel monitor.  provides easy visibility
    into a bunch of stuff which is otherwise kind of hard to find
- [`lm_sensors`](https://wiki.archlinux.org/index.php/Lm_sensors) just what it sounds
    like, access to sensors.  use `sensors-detect` and `sensors`.
- [`conky`](https://github.com/brndnmtthws/conky) a flexible C++ program for displaying
    system information. Can also be displayed in background of i3 without its own window.
    ***TODO*** make a config for this!
- [`piper`](https://github.com/libratbag/piper) graphical tool for configuring input devices such as
    gaming mice.
- [`lazydocker`](https://github.com/jesseduffield/lazydocker) a TUI for managing dockers. Many
    common docker operations are a huge pain in the ass without it.

## Media
- [`mpv`](https://wiki.archlinux.org/index.php/Mpv) a very minimalistic media player.
    great when used from other applications
- [`vlc`](https://www.videolan.org/vlc/) the original
- [`zathura`](https://wiki.archlinux.org/index.php/Zathura) vim-like PDF reader
- [`scrot`](https://wiki.archlinux.org/index.php/Screen_capture) for taking screenshots
- [`flameshot`](https://github.com/flameshot-org/flameshot) the most elaborate screen capture tool
- [`peek`](https://github.com/phw/peek) for recording screen `gif`s. written in some
    obscure C#-like language called Vala
- [`alsamixer`](https://wiki.archlinux.org/index.php/Sound_system) a TUI for adjusting
    sound
- [`PulseAudio`](https://wiki.archlinux.org/index.php/PulseAudio) linux sound middleware.
    pretty powerful, you should probably use it.  has lots of front-ends
- [`castero`](https://github.com/xgi/castero) a terminal based podcast player.

## Games
- [`lutris`](https://lutris.net/) aggregates ways of launching games on linux: steam, epic, gog,
    emulators, native games, everything.
- [`GluriousEggroll`](https://github.com/GloriousEggroll/proton-ge-custom) is some dude that
    compiles proton with all bleeding edge dependencies.  Easy to install and can do some seemingly
    miraculous fixes.

## Mobile
At some point this will get its own dotfiles, but for now...
- [`sxmo`](https://sxmo.org/) postmarketOS/alpine distribution for phones.

