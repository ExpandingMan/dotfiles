try
    using Revise
catch e
    @warn("error initializing revise", exception=(e, catch_backtrace()))
end

try
    using TerminalPager
catch e
end
try
    using QuickMenus
catch e
end

try
    using OhMyREPL
catch e
end

atreplinit() do repl
    if @isdefined OhMyREPL
        OhMyREPL.colorscheme!("Dracula")
        OhMyREPL.input_prompt!("◖◗ ", :cyan)
        # this interferes with too many things such as nvim REPL interaction
        OhMyREPL.enable_autocomplete_brackets(false)
    end
end
