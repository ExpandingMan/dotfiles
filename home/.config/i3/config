## dracula color definitions
set $back2black          #000000
set $dracula_background  #282a36
set $dracula_currentline #44475a
set $dracula_selection   #44475a
set $dracula_foreground  #f8f8f2
set $dracula_comment     #6272a4
set $dracula_cyan        #8be9fd
set $dracula_green       #50fa7b
set $dracula_orange      #ffb86c
set $dracula_pink        #ff79c6
set $dracula_purple      #bd93f9
set $dracula_red         #ff5555
set $dracula_yellow      #f1fa8c


# NOTE: this for interactive keybindings list; it's the only one you can't forget!
bindsym $mod+Shift+y exec $HOME/sbin/rofi/i3keybindings.sh


# ---------------------------------------- color config -------------------------------------------------------------------
# colors                    border               back                text                 indicator            child_border
client.focused              $back2black          $dracula_background $dracula_cyan        $dracula_background  $dracula_background
client.focused_inactive     $back2black          $dracula_background $dracula_comment     $dracula_background  $dracula_background
client.unfocused            $back2black          $back2black         $dracula_comment     $dracula_background  $dracula_background
client.urgent               $dracula_red         $dracula_background $dracula_red         $dracula_red         $dracula_background
client.placeholder          $back2black          $back2black         $dracula_orange      $dracula_orange      $dracula_background
client.background           $back2black
# ---------------------------------------- color config -------------------------------------------------------------------

smart_gaps on
#gaps inner 4

# set modifier button to win/os button
set $mod Mod4

# set font
font pango:JuliaMono, Regular 8

#----------------------------------------------------- display -------------------------------------------------------------
# set background video and background

exec_always --no-startup-id "~/.screenlayout/default.sh"
exec_always --no-startup-id "sleep 1" # sometimes feh gets confused without this
exec_always --no-startup-id "~/sbin/bgrandom"

# start picom
exec_always --no-startup-id "picom --config $HOME/.config/picom/picom.conf"

# arch wiki recommends doing this even though it auto-starts throug dbus
exec_always --no-startup-id "dunst"

# start blueman bluetooth manager
exec_always --no-startup-id "blueman-applet"

# binding for random backgrounds
bindsym $mod+Shift+b exec --no-startup-id ~/sbin/bgrandom

# binding for bpytop
bindsym $mod+Shift+t exec --no-startup-id ~/sbin/top
#----------------------------------------------------- display -------------------------------------------------------------

# start gnome-keyring which may be required for stuff like protonvpn
exec --no-startup-id /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
exec --no-startup-id /usr/bin/gnome-keyring-daemon --start

# this NetworkManager thingy is needed for NetworkManager stuff like VPN
exec --no-startup-id nm-applet

#-------------------------------------------------- special workspaces -----------------------------------------------------
assign [instance="bpytop"] top
assign [class="(?i)firefox"] browser
assign [class="Steam"] steam
assign [class="FreeTube"] video

# this is not ideal, but title is set too late for that to work; if using other pop-ups will have to
# change.  Consider using `for_window` as an alternative
# order matters here because it matches the first statement first
assign [class="Brave" window_role="pop-up"] video
assign [class="Brave"] browser

# this attempts to assign all games to the "game" workspace by checking their class mentions steam
# obviously this will not work on non-steam games
assign [class="steam_app"] game

# always make steam floating
for_window [class="Steam"] floating enable

# go to system monitor workspace
bindsym $mod+F1 workspace top

# move window to system monitor workspace
bindsym $mod+Shift+F1 move container to workspace top

# go to browser workspace
bindsym $mod+F2 workspace browser

# move window to browser workspace
bindsym $mod+Shift+F2 move container to workspace browser

# go to steam workspace
bindsym $mod+F3 workspace steam

# move window to steam workspace
bindsym $mod+Shift+F3 move container to workspace steam

# go to video player (e.g. freetube) workspace
bindsym $mod+F4 workspace video

# move window to video player workspace
bindsym $mod+Shift+F4 move container to workspace video

# go to game workspace
bindsym $mod+F5 workspace game

# move window to game workspace
bindsym $mod+Shift+F5 move container to workspace game
#-------------------------------------------------- special workspaces -----------------------------------------------------


#------------------------------------------------------ terminal ------------------------------------------------------------
exec --no-startup-id "export TERMINAL=alacritty"

# run terminal
bindsym $mod+Return exec alacritty
#------------------------------------------------------ terminal ------------------------------------------------------------


# launch polybar
exec_always --no-startup-id $HOME/.config/polybar/launch.sh

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# this makes the borders look a little prettier.  Requires i3_gaps
smart_borders on
smart_borders no_gaps

# monitor setup... TODO: need to make generic
workspace 1 output DP-0
workspace 2 output DP-2


#------------------------------------------------------ keybindings --------------------------------------------------------
# reload the configuration file
bindsym $mod+Shift+c reload

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart

# kill focused window
bindsym $mod+Shift+q kill

# rofi run menu
bindsym $mod+d exec --no-startup-id "rofi -show run -1"

# rofi window menu
bindsym $mod+backslash exec --no-startup-id "rofi -show window -1"

# rofi ssh menu
bindsym $mod+Shift+d exec --no-startup-id "rofi -show ssh -1"

# close last dunst notification
bindsym $mod+n exec --no-startup-id "dunstctl close"

# close all dunst notifications
bindsym $mod+Shift+n --no-startup-id "dunstctl close-all"

# take action on latest dunst notification
bindsym $mod+Ctrl+n --no-startup-id "dunstctl action 0"

# focus left
bindsym $mod+h focus left

# focus down
bindsym $mod+j focus down

# focus up
bindsym $mod+k focus up

# focus right
bindsym $mod+l focus right

# focus left
bindsym $mod+Left focus left

# focus down
bindsym $mod+Down focus down

# focus up
bindsym $mod+Up focus up

# focus right
bindsym $mod+Right focus right

# move window left
bindsym $mod+Shift+h move left

# move window down
bindsym $mod+Shift+j move down

# move window up
bindsym $mod+Shift+k move up

# move window right
bindsym $mod+Shift+l move right

# move window left
bindsym $mod+Shift+Left move left

# move window down
bindsym $mod+Shift+Down move down

# move window up
bindsym $mod+Shift+Up move up

# move window right
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+y split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# container layout stacked
bindsym $mod+s layout stacking

# container layout tabbed
bindsym $mod+w layout tabbed

# container layout split
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
bindsym $mod+z focus child

# workspace 10
bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10

# move container to workspace 10
bindsym $mod+Shift+1 move container to workspace 1
bindsym $mod+Shift+2 move container to workspace 2
bindsym $mod+Shift+3 move container to workspace 3
bindsym $mod+Shift+4 move container to workspace 4
bindsym $mod+Shift+5 move container to workspace 5
bindsym $mod+Shift+6 move container to workspace 6
bindsym $mod+Shift+7 move container to workspace 7
bindsym $mod+Shift+8 move container to workspace 8
bindsym $mod+Shift+9 move container to workspace 9
bindsym $mod+Shift+0 move container to workspace 10

# move the currently focused window to the scratchpad
bindsym $mod+Shift+minus move scratchpad

# show next scratchpad window, or hide focused scratchpad window
bindsym $mod+minus scratchpad show

# Set shut down, restart and locking features
bindsym $mod+Escape mode "$mode_system"

set $mode_system (l)ock, (e)xit, switch_(u)ser, (s)uspend, (h)ibernate, (r)eboot, (Shift+s)hutdown
mode "$mode_system" {
    bindsym l exec --no-startup-id "fish -c 'i3exit lock'", mode "default"
    bindsym s exec --no-startup-id "fish -c 'i3exit suspend'", mode "default"
    bindsym u exec --no-startup-id "fish -c 'i3exit switch_user'", mode "default"
    bindsym e exec --no-startup-id "fish -c 'i3exit logout'", mode "default"
    bindsym h exec --no-startup-id "fish -c 'i3exit hibernate'", mode "default"
    bindsym r exec --no-startup-id "fish -c 'i3exit reboot'", mode "default"
    bindsym Shift+s exec --no-startup-id "fish -c 'i3exit shutdown'", mode "default"

    bindsym Return mode "default"
    bindsym Escape mode "default"
}

mode "resize" {
        # (resize mode) shrink window left
        bindsym h resize shrink width 10 px or 10 ppt

        # (resize mode) grow window up
        bindsym j resize grow height 10 px or 10 ppt

        # (resize mdoe) shrink window down
        bindsym k resize shrink height 10 px or 10 ppt

        # (resize mode) grow window right
        bindsym l resize grow width 10 px or 10 ppt

        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        bindsym Return mode "default"
        bindsym Escape mode "default"
}

# resize mode
bindsym $mod+r mode "resize"

# raise volume
bindsym XF86AudioRaiseVolume exec "pamixer --unmute -i 5"

# lower volume
bindsym XF86AudioLowerVolume exec "pamixer --unmute -d 5"

# audio mute
bindsym XF86AudioMute exec "pamixer -t"

# play audio
bindsym XF86AudioPlay exec "playerctl play-pause"

# audio next
bindsym XF86AudioNext exec "playerctl next"

# audio previous
bindsym XF86AudioPrev exec "playerctl previous"

# take screenshot of entire monitor output
bindsym Print exec "scrot -e 'mv $f ~/Pictures/screenshots'"

# take screenshot of current window
bindsym $mod+Print exec "scrot -u -e 'mv $f ~/Pictures/screenshots'"

# take screenshot of selectable box (with mouse)
bindsym --release Shift+Print exec "scrot -s -e 'mv $f ~/Pictures/screenshots'"

# use Peek to record a gif
bindsym Ctrl+Print exec "peek"

# lock the screen
bindsym $mod+Shift+x exec "i3lock -c 000000 -n"

# move workspace to the monitor to the right
bindsym $mod+Shift+greater move workspace to output right

# move workspace to the monitor to the left
bindsym $mod+Shift+less move workspace to output left

# move to urgent window
bindsym $mod+x [urgent=latest] focus

# bring up my moonlander keyboard layout
bindsym $mod+Shift+m exec "$BROWSER https://configure.zsa.io/moonlander/layouts/NpwY6/latest/"

##--- rofi menus ---##

# volume menu (up, down, j, k, m for mute)
bindsym $mod+Shift+v exec --no-startup-id ~/sbin/rofi/volume.sh

#-------------------------------------------------------- keybindings --------------------------------------------------------
