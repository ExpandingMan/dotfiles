#!/bin/bash

#=======================================================================================================
#  attempt to check whether there is an active VPN connection by checking NetworkManager nmcli
#=======================================================================================================

NMCLI_CMD="nmcli -g type conn show --active"

o=""
for line in `$NMCLI_CMD`; do
    if [[ $line =~ "vpn" ]] || [[ $line =~ "tun" ]]; then
        o="◱"
    fi
done

echo $o
