#!/bin/bash

# Terminate already running bar instances
killall -q polybar

# determine network interface
# ethernet starts wit en, wifi starts with wl
# WARNING not yet sure if this will always work
export NETWORK_INTERFACE=`ip -o -4 route show to default | awk '/dev .*/{print $5}'`
# try to check if this is wifi
if [[ $NETWORK_INTERFACE =~ wl ]]; then
    export NETWORK_INTERFACE_WIFI=$NETWORK_INTERFACE
    unset NETWORK_INTERFACE
fi

# Wait until the processes have been shut down
while pgrep -x polybar >/dev/null; do sleep 1; done

# Launch bar
# Bar is the name set in the polybar config, so if you change it, you have to change it here too.
# polybar bar

# launch for all monitors
if type "xrandr"; then
    for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
        MONITOR=$m polybar --reload bar &
    done
else
    polybar --reload bar
fi

echo "Bars launched..."
