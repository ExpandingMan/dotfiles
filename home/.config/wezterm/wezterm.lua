local WezTerm = require("wezterm")
local act = WezTerm.action

return {
    -- I've mostly switched to nu but am still terrified of chsh-ing it
    default_prog = {"/sbin/nu"},

    -- the WebGpu front-end uses vulkan; currently still has somewhat wonky text rendering
    front_end = "WebGpu",
    webgpu_power_preference = "HighPerformance",
    enable_tab_bar = false,
    color_scheme = "DraculaCorrect",
    font = WezTerm.font("JuliaMono-Regular"),
    default_cursor_style = "BlinkingBar",
    cursor_blink_rate = 400,
    cursor_thickness = "1.5pt",
    font_rules = {
        {italic=true, font=WezTerm.font("JuliaMono-MediumItalic")},
        {intensity="Bold", font=WezTerm.font("JuliaMono-Bold")},
        {intensity="Bold", italic=true, font=WezTerm.font("JuliaMono-BoldItalic")},
    },
    font_size = 12.0,
    window_padding = {
        left = 0,
        right = 0,
        top = 0,
        bottom = 0,
    },

    adjust_window_size_when_changing_font_size = false,

    warn_about_missing_glyphs = false, -- this pops when I can find nothing wrong

    check_for_updates = false,  -- normally using pacman so this is pointless

    disable_default_key_bindings = true,
    keys = {
        {mods="CTRL|SHIFT", key="c", action=act{CopyTo="Clipboard"}},
        {mods="CTRL|SHIFT", key="v", action=act{PasteFrom="Clipboard"}},
        {mods="ALT|SHIFT", key="+", action="IncreaseFontSize"},  -- this is ALT+
        {mods="ALT", key="-", action="DecreaseFontSize"},
        {mods="ALT", key="=", action="ResetFontSize"},
        {mods="ALT", key="/", action=act.Search("CurrentSelectionOrEmptyString")},
        {mods="ALT|SHIFT", key="/", action=act.Search{CaseSensitiveString=""}},
        {mods="ALT|SHIFT", key="r", action="ReloadConfiguration"},
        -- this is like vim normal mode, hence the key binding
        {mods="ALT", key="n", action="ActivateCopyMode"},
        {mods="ALT|CTRL", key="u", action=act{ScrollByPage=-1}},
        {mods="ALT|CTRL", key="d", action=act{ScrollByPage=1}},
        {mods="ALT", key="k", action=act{ScrollByLine=-1}},
        {mods="ALT|SHIFT", key="k", action=act{ScrollByLine=-5}},
        {mods="ALT", key="j", action=act{ScrollByLine=1}},
        {mods="ALT|SHIFT", key="j", action=act{ScrollByLine=5}},
    },

    key_tables = {
        copy_mode = {
            --NOTE alt mouse drag should also work for block selection
            {mods="NONE", key="i", action=act{CopyMode="Close"}},
            {mods="NONE", key="a", action=act{CopyMode="Close"}},
            {mods="NONE", key="o", action=act{CopyMode="Close"}},
            {mods="NONE", key="h", action=act{CopyMode="MoveLeft"}},
            {mods="NONE", key="j", action=act{CopyMode="MoveDown"}},
            {mods="NONE", key="k", action=act{CopyMode="MoveUp"}},
            {mods="NONE", key="l", action=act{CopyMode="MoveRight"}},
            {mods="NONE", key="w", action=act{CopyMode="MoveForwardWord"}},
            {mods="NONE", key="e", action=act{CopyMode="MoveForwardWordEnd"}},
            {mods="NONE", key="b", action=act{CopyMode="MoveBackwardWord"}},
            {mods="NONE", key="0", action=act{CopyMode="MoveToStartOfLine"}},
            {mods="SHIFT", key="phys:4", action=act{CopyMode="MoveToEndOfLineContent"}},
            {mods="NONE", key="Enter", action=act{CopyMode="MoveToViewportMiddle"}},
            {mods="NONE", key="v", action=act{CopyMode={SetSelectionMode="Cell"}}},
            {mods="CTRL", key="y", action=act{CopyTo="ClipboardAndPrimarySelection"}},
            {mods="NONE", key="y", action=act{Multiple={
                act{CopyTo="ClipboardAndPrimarySelection"},
                act{CopyMode="Close"},
            }}},
            {mods="CTRL", key="v", action=act{CopyMode={SetSelectionMode="Block"}}},
            {mods="NONE", key="/", action=act.Search("CurrentSelectionOrEmptyString")},
            {mods="CTRL", key="u", action=act{CopyMode="ClearPattern"}},
            {mods="ALT|CTRL", key="u", action=act{ScrollByPage=-1}},
            {mods="ALT|CTRL", key="d", action=act{ScrollByPage=1}},
            {mods="CTRL", key="k", action=act{ScrollByLine=-1}},
            {mods="CTRL|SHIFT", key="k", action=act{ScrollByLine=-5}},
            {mods="CTRL", key="j", action=act{ScrollByLine=1}},
            {mods="CTRL|SHIFT", key="j", action=act{ScrollByLine=5}},
        },
        search_mode = {
            {mods="NONE", key="Escape", action=act{CopyMode="Close"}},
            {mods="CTRL", key="n", action=act{CopyMode="NextMatch"}},
            {mods="CTRL|SHIFT", key="n", action=act{CopyMode="PriorMatch"}},
            {mods="CTRL", key="u", action=act{CopyMode="ClearPattern"}},
        },
    }
}
