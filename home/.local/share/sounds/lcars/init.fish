#!/usr/bin/fish
test -d stereo || mkdir stereo

set FROM $HOME/Audio/lcars
set TO (dirname (status -f))/stereo

function cps
    cp $FROM/$argv[1].wav $TO/$argv[2].wav
end

# login/logout
cps 220 service-login
cps 227 service-logout
cps 212 network-connectivity-established

# basic notifications
cps 207 bell
cps 203 message
cps 215 complete

# window notifications
cps 205 window-question
cps 225 dialog-error
cps 218 dialog-information

# screen capture
cps 210 camera-shutter
cps 210 screen-capture
