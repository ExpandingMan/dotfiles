#import "@preview/touying:0.5.5": *

#let d-background = rgb("#282a36")
#let d-selection = rgb("#44475a")
#let d-foreground = rgb("#f8f8f2")
#let d-comment = rgb("#6272a4")
#let d-cyan = rgb("#8be9fd")
#let d-green = rgb("#50fa7b")
#let d-orange = rgb("#ffb86c")
#let d-pink = rgb("#ff79c6")
#let d-purple = rgb("#bd93f9")
#let d-red = rgb("#ff5555")
#let d-yellow = rgb("#f1fa8c")

// these are util functions we provide for more easily coloring text;
// which is something particularly nice to do in presentations
#let comment(txt) = text(fill: d-comment, txt)
#let cyan(txt) = text(fill: d-cyan, txt)
#let green(txt) = text(fill: d-green, txt)
#let orange(txt) = text(fill: d-orange, txt)
#let pink(txt) = text(fill: d-pink, txt)
#let purple(txt) = text(fill: d-purple, txt)
#let red(txt) = text(fill: d-red, txt)
#let yellow(txt) = text(fill: d-yellow, txt)

#let slide(
  config: (:),
  repeat: auto,
  setting: body => body,
  composer: auto,
  ..bodies,
) = touying-slide-wrapper(self => {
  let deco-format(it) = text(size: .6em, fill: gray, it)
  let header(self) = deco-format(
    components.left-and-right(
      utils.call-or-display(self, self.store.header),
      utils.call-or-display(self, self.store.header-right),
    ),
  )
  let footer(self) = {
    v(.5em)
    deco-format(
      components.left-and-right(
        utils.call-or-display(self, self.store.footer),
        utils.call-or-display(self, self.store.footer-right),
      ),
    )
  }
  let self = utils.merge-dicts(
    self,
    config-page(
      header: header,
      footer: footer,
    ),
    config-common(subslide-preamble: self.store.subslide-preamble),
  )
  touying-slide(self: self, config: config, repeat: repeat, setting: setting, composer: composer, ..bodies)
})


/// Centered slide for the presentation.
/// 
/// - config (dictionary): The configuration of the slide. You can use `config-xxx` to set the configuration of the slide. For more several configurations, you can use `utils.merge-dicts` to merge them.
#let centered-slide(config: (:), ..args) = touying-slide-wrapper(self => {
  touying-slide(self: self, ..args.named(), config: config, align(center + horizon, args.pos().sum(default: none)))
})


/// Title slide for the presentation.
///
/// Example: `#title-slide[Hello, World!]`
/// 
/// - config (dictionary): The configuration of the slide. You can use `config-xxx` to set the configuration of the slide. For more several configurations, you can use `utils.merge-dicts` to merge them.
#let title-slide(config: (:), body) = centered-slide(
  config: utils.merge-dicts(config, config-common(freeze-slide-counter: true)),
  body,
)


/// New section slide for the presentation. You can update it by updating the `new-section-slide-fn` argument for `config-common` function.
/// 
/// - config (dictionary): The configuration of the slide. You can use `config-xxx` to set the configuration of the slide. For more several configurations, you can use `utils.merge-dicts` to merge them.
#let new-section-slide(config: (:), body) = centered-slide(config: config, [
  #text(1.2em, weight: "bold", utils.display-current-heading(level: 1))

  #body
])


/// Focus on some content.
///
/// Example: `#focus-slide[Wake up!]`
/// 
/// - config (dictionary): The configuration of the slide. You can use `config-xxx` to set the configuration of the slide. For more several configurations, you can use `utils.merge-dicts` to merge them.
/// 
/// - background (color, auto): The background color of the slide. Default is `auto`, which means the primary color of the slides.
/// 
/// - foreground (color): The foreground color of the slide. Default is `white`.
#let focus-slide(config: (:), background: auto, foreground: white, body) = touying-slide-wrapper(self => {
  self = utils.merge-dicts(
    self,
    config-common(freeze-slide-counter: true),
    config-page(fill: if background == auto {
      self.colors.primary
    } else {
      background
    }),
  )
  set text(fill: foreground, size: 1.5em)
  touying-slide(self: self, config: config, align(center + horizon, body))
})


/// Touying simple theme.
///
/// Example:
///
/// ```typst
/// #show: simple-theme.with(aspect-ratio: "16-9", config-colors(primary: blue))`
/// ```
///
/// The default colors:
///
/// ```typst
/// config-colors(
///   neutral-light: gray,
///   neutral-lightest: rgb("#ffffff"),
///   neutral-darkest: rgb("#000000"),
///   primary: aqua.darken(50%),
/// )
/// ```
///
/// - aspect-ratio (string): The aspect ratio of the slides. Default is `16-9`.
///
/// - header (function): The header of the slides. Default is `self => utils.display-current-heading(setting: utils.fit-to-width.with(grow: false, 100%), depth: self.slide-level)`.
///
/// - header-right (content): The right part of the header. Default is `self.info.logo`.
///
/// - footer (content): The footer of the slides. Default is `none`.
///
/// - footer-right (content): The right part of the footer. Default is `context utils.slide-counter.display() + " / " + utils.last-slide-number`.
///
/// - primary (color): The primary color of the slides. Default is `aqua.darken(50%)`.
///
/// - subslide-preamble (content): The preamble of the subslides. Default is `block(below: 1.5em, text(1.2em, weight: "bold", utils.display-current-heading(level: 2)))`.
#let dracula-theme(
  aspect-ratio: "16-9",
  header: none,
  header-right: self => self.info.logo,
  footer: none,
  footer-right: context comment(utils.slide-counter.display() + " / " + utils.last-slide-number),
  subslide-preamble: block(
    below: 1.5em,
    text(1.2em, weight: "bold", utils.display-current-heading(level: 2)),
  ),
  ..args,
  body,
) = {
  set text(fill: d-foreground)
  show: touying-slides.with(
    config-page(
      paper: "presentation-" + aspect-ratio,
      margin: 2em,
      footer-descent: 0em,
      fill: d-background,
    ),
    config-common(
      slide-fn: slide,
      new-section-slide-fn: new-section-slide,
      zero-margin-header: false,
      zero-margin-footer: false,
    ),
    config-methods(
      init: (self: none, body) => {
        set text(size: 25pt)
        show footnote.entry: set text(size: .6em)
        show heading.where(level: 1): set text(1.4em)

        body
      },
      alert: utils.alert-with-primary-color,
    ),
    config-colors(
      neutral: d-foreground,
      neutral-light: d-foreground,
      neutral-lighter: d-foreground,
      neutral-lightest: d-foreground,
      neutral-dark: d-selection,
      neutral-darker: d-selection,
      neutral-darkest: d-selection,
      primary: d-cyan,
      primary-light: d-cyan,
      primary-lighter: d-cyan,
      primary-lightest: d-cyan,
      primary-dark: d-orange,
      primary-darker: d-orange,
      primary-darkest: d-orange,
      secondary: d-green,
      secondary-light: d-green,
      secondary-lighter: d-green,
      secondary-lightest: d-green,
      secondary-dark: d-purple,
      secondary-darker: d-purple,
      secondary-darkest: d-purple,
      tertiary: d-yellow,
      tertiary-light: d-yellow,
      tertiary-lighter: d-yellow,
      tertiary-lightest: d-yellow,
      tertiary-dark: d-red,
      tertiary-darker: d-red,
      tertiary-darkest: d-red,
    ),
    // save the variables for later use
    config-store(
      header: header,
      header-right: header-right,
      footer: footer,
      footer-right: footer-right,
      subslide-preamble: subslide-preamble,
    ),
    ..args,
  )

  body
}
