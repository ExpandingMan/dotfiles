#import "@preview/polylux:0.3.1": *

#let d-background = rgb("#282a36")
#let d-selection = rgb("#44475a")
#let d-foreground = rgb("#f8f8f2")
#let d-comment = rgb("#6272a4")
#let d-cyan = rgb("#8be9fd")
#let d-green = rgb("#50fa7b")
#let d-orange = rgb("#ffb86c")
#let d-pink = rgb("#ff79c6")
#let d-purple = rgb("#bd93f9")
#let d-red = rgb("#ff5555")
#let d-yellow = rgb("#f1fa8c")


// these are util functions we provide for more easily coloring text;
// which is something particularly nice to do in presentations
#let comment(txt) = text(fill: d-comment, txt)
#let cyan(txt) = text(fill: d-cyan, txt)
#let green(txt) = text(fill: d-green, txt)
#let orange(txt) = text(fill: d-orange, txt)
#let pink(txt) = text(fill: d-pink, txt)
#let purple(txt) = text(fill: d-purple, txt)
#let red(txt) = text(fill: d-red, txt)
#let yellow(txt) = text(fill: d-yellow, txt)


//TODO: create code highlighting theme

#let d-footer = state("d-footer", [])

#let d-cell = block.with(
  width: 100%,
  height: 100%,
  above: 0pt,
  below: 0pt,
  breakable: false
)

#let d-progress-bar = utils.polylux-progress( ratio => {
  grid(
    columns: (ratio * 100%, 1fr),
    d-cell(fill: d-comment),
    d-cell(fill: d-selection),
  )
})

#let dracula-theme(
    aspect-ratio: "16-9",
    font: "New Computer Modern",
    font-size: 25pt,
    footer: [],
    body
  ) = {
  set page(
    paper: "presentation-" + aspect-ratio,
    fill: d-background,
    margin: 0em,
    header: none,
    footer: none,
  )

  set text(font: font, size: font-size)

  set list(marker: (text(fill: d-pink)[•]))

  set enum(numbering: x => text(fill: d-pink, [#x.]))

  d-footer.update(footer)

  body
}

#let title-slide(
  title: [],
  subtitle: none,
  author: none,
  date: none,
  extra: none,
) = {
  let content = {
    set text(fill: d-foreground)
    set align(horizon)
    block(width: 100%, inset: 2em, {
      text(size: 1.3em, fill: d-cyan, strong(title))
      if subtitle != none {
        linebreak()
        text(size: 0.9em, subtitle, fill: d-comment)
      }
      line(length: 100%, stroke: 0.05em + d-selection)
      set text(size: .8em)
      if author != none {
        block(spacing: 1em, text(fill: d-green, author))
      }
      if date != none {
        block(spacing: 1em, date)
      }
      set text(size: 0.8em)
      if extra != none {
        block(spacing: 1em, extra)
      }
    
    })
  }

  logic.polylux-slide(content)
}

#let slide(title: none, body) = {
  let header = {
    set align(top)
    if title != none {
      show: d-cell.with(fill: d-selection, inset: 1em)
      set align(horizon)
      set text(fill: d-purple, size: 1.2em)
      strong(title)
    } else { [] }
  }

  let footer = {
    set text(size: 0.8em)
    set align(bottom)
    text(fill: d-comment, d-footer.display())
    h(1fr)
    text(fill: d-comment, size: 0.6em)[
      #logic.logical-slide.display() / #utils.last-slide-number #h(1em)
    ]
    block(height: 4pt, width: 100%, spacing: 0pt, d-progress-bar)
  }

  set page(
    header: header,
    footer: footer,
    margin: (top: 2.5em, bottom: 1em),
    fill: d-background,
  )

  let content = {
    show: align.with(horizon)
    show: pad.with(2em)
    set text(fill: d-foreground)
    body 
  }

  logic.polylux-slide(content)
}

#let new-section-slide(name) = {
  let content = {
    utils.register-section(name)
    set align(horizon)
    show: pad.with(20%)
    set text(size: 1.5em, fill: d-orange)
    name
    block(height: 2pt, width: 100%, spacing: 0pt, d-progress-bar)
  }
  logic.polylux-slide(content)
}

#let focus-slide(body) = {
  set page(fill: d-selection, margin: 2em)
  set text(fill: d-red, size: 1.5em)
  logic.polylux-slide(align(horizon + center, body))
}

#let alert = text.with(fill: d-red)

#let dracula-outline = utils.polylux-outline(enum-args: (tight: false,))
