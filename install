#!/bin/bash
INSTALLER_URL="https://gitlab.com/ExpandingMan/Installer.jl"


print_help() {
    echo """
    Usage: install
        -h, --help      print this message
        -u, --update    update the installer repository (latest master)

    Runs the installer program.  This runs in Julia, julia binaries should be installed
    with juliaup.
    """
}

update_installer() {
    git -C "$(dirname $0)/Installer" pull origin master
}


# option parsing loop
while [[ "$1" =~ ^- && ! "$1" == "--" ]]; do case $1 in
    -h | --help )
        print_help
        exit 0
        ;;
    -u | --update )
        update_installer
        ;;
esac; shift; done
if [[ $1 == '--' ]]; then shift; fi

# install Julia if first argument is 'julia'
#NOTE: the exits so you don't inadvertantly compile as sudo
if [[ $1 == 'julia' ]]; then install_julia; exit 0; fi

# check if we have Installer.jl
if [[ ! -e "$(dirname $0)/Installer" ]]; then
    echo "... Installer.jl not present, cloning ..."
    git -C $(dirname $0) clone $INSTALLER_URL Installer
fi

# variables used by installer
export INSTALLER_DOTFILES_DIR=$(dirname $0)/home
export INSTALLER_PROGRAMS_DIR=$(dirname $0)/programs


# run the installer
$(dirname $0)/Installer/run
