
const qtile = Program(:qtile,
                      repos=[GitRepo("git@gitlab.com:ExpandingMan/qtileconfig.git",
                                     joinpath(I.homedir(),".config","qtile"),
                                     fallback_url="https://gitlab.com/ExpandingMan/qtileconfig"
                                    ),
                            ]
                     )
