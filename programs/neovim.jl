
const neovim = Program(:neovim,
                       repos=[GitRepo("git@gitlab.com:ExpandingMan/neovimconfig.git",
                                      joinpath(I.homedir(),".config","nvim"),
                                      fallback_url="https://gitlab.com/ExpandingMan/neovimconfig"
                                     ),
                             ]
                      )

neovim_appimage_url(::Val{:latest}) = "https://github.com/neovim/neovim/releases/download/v0.5.0/nvim.appimage"
neovim_appimage_url(::Val{:nightly}) = "https://github.com/neovim/neovim/releases/download/nightly/nvim.appimage"

function aux(p::Program{:neovim}; confirm::Bool=isinteractive(), appimage=:nightly)
    # get AppImage unless FUSE is unavailable
    Downloads.download(Download(neovim_appimage_url(Val(appimage)), "sbin/nvim", true))

    # we run this here to make sure that `which nvim` is evaluated *last*
    nvim = String(readchomp(`which nvim`))
    # note you need a separate `-c` for each command
    I.runcommand(Cmd([nvim, "--headless", "-c", "autocmd User PackerComplete quitall", "-c", "PackerSync"]); confirm)
end
