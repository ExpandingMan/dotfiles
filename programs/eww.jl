
_eww_repo_directory() = joinpath(I.homedir(),"src","eww")

const eww = Program(:eww,
                    repos=[GitRepo("git@github.com:elkowar/eww.git",
                                   _eww_repo_directory(),
                                   fallback_url="https://github.com/elkowar/eww",
                                   tag="v0.4.0",
                                  ),
                          ]
                   )

function aux(p::Program{:eww}; confirm::Bool=isinteractive())
    dir = _eww_repo_directory()
    cd(dir) do
        run(`cargo build --release`)
    end
    exe = joinpath(dir, "target", "release", "eww")
    run(`chmod a+x $exe`)
    link = expanduser("~/sbin/eww")
    run(`ln -s $exe $link`)
end
