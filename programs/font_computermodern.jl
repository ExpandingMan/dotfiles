
@isdefined(FONT_DIR) || (const FONT_DIR = joinpath(ENV["HOME"],".local","share","fonts"))

#WARN: consider installing otf-latin-modern and otf-latinmodern-math, which typst can also use

const font_computermodern = Program(:font_computermodern,
                                    commands=[Cmd(`rm -rf Computer-Modern`, dir=FONT_DIR),
                                              Cmd(`git clone https://github.com/spratt/Computer-Modern`, dir=FONT_DIR),
                                              Cmd(`fc-cache`),
                                            ]
                                   )
