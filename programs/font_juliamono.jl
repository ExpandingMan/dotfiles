
@isdefined(FONT_DIR) || (const FONT_DIR = joinpath(ENV["HOME"],".local","share","fonts"))

const font_juliamono = Program(:font_juliamono,
                               commands=[Cmd(`rm -rf juliamono`, dir=FONT_DIR),
                                         Cmd(`git clone --depth 1 --branch v0.054 https://github.com/cormullion/juliamono`, dir=FONT_DIR),
                                         # these can cause problems, but may need to remove this
                                         Cmd(`rm JuliaMono-BoldLatin.ttf JuliaMono-RegularLatin.ttf`, dir=joinpath(FONT_DIR,"juliamono")),
                                         Cmd(`fc-cache`),
                                        ]
                              )
