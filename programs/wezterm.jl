
wezterm_appimage_url() = "https://github.com/wez/wezterm/releases/download/nightly/WezTerm-nightly-Ubuntu18.04.AppImage"

const wezterm = Program(:wezterm,
                        configs=[".config/wezterm/", ".local/share/applications/wezterm.desktop"],
                        downloads=[Download(wezterm_appimage_url(), "sbin/wezterm", true)],
                       )
