#TODO: still need to set cursor in .Xresources and .config/gtk-3.0/settings.ini

const cursors_future = Program(:cursors_future)

_future_cursors_repo() = "https://github.com/yeyushengfan258/Future-cursors"

_future_cursors_theme_config() = """
[Icon Theme]
Name=Future_Cursors
"""

function aux(::Program{:cursors_future}; confirm::Bool=isinteractive())
    repo = _future_cursors_repo()

    path = joinpath(I.homedir(),".icons")
    ispath(path) || mkpath(path)

    cd(path) do
        rm("_cursors_future_tmp"; recursive=true, force=true)
        I.runcommand(`git clone $repo _cursors_future_tmp`; confirm)
        cp(joinpath("_cursors_future_tmp","dist"), "Future_Cursors"; force=true)

        thf = joinpath("Future_Cursors", "index.theme")
        rm(thf; force=true)
        open(io -> write(io, _future_cursors_theme_config()), thf; write=true)
        
        rm("_cursors_future_tmp"; recursive=true, force=true)
    end
end
