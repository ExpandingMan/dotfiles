
const gtk = Program(:gtk,
                    configs=[".config/gtk-3.0/settings.ini"],
                   )

function aux(p::Program{:gtk}; confirm::Bool=isinteractive())
    @info("GTK configures GUI elements. Be sure to also install `cursors_hacked`.\n"*
          "You may need to configure via `lxappearance`."
         )
    if I.issudo()
        cd("/usr/share/themes") do
            isdir("Dracula") && I.runcommand(`rm -r Dracula`; confirm)
            I.runcommand(`git clone https://github.com/dracula/gtk Dracula`; confirm)
        end
    else
        @warn("sudo is required for cloning theme into `/usr/share/themes/`")
    end
end
