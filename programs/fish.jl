
const fish = Program(:fish,
                     repos=[GitRepo("git@gitlab.com:ExpandingMan/fishconfig.git",
                                    joinpath(I.homedir(),".config","fish"),
                                    fallback_url="https://gitlab.com/ExpandingMan/fishconfig",
                                   ),
                           ]
                    )
